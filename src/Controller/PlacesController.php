<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Category;
use App\Entity\Place;

class PlacesController extends AbstractController
{
    /**
     * @Route("/places/{id}", name="place_id", methods="GET")
     */
    public function place_id(Place $place)
    {
        if(!is_null($place)){
            $data = [
                'id' => $place->getId(),
                'category_id' => $place->getCategory()->getId(), 
                'name' => $place->getName(),
                'address' => $place->getAddress(),
                'postal_code' => $place->getPostalCode(),
                'city' => $place->getCity(),
                'latitude' => $place->getLatitude(),
                'longitude' => $place->getLongitude()
            ];
            $code = JsonResponse::HTTP_OK;
        }

        $json = json_encode($data, JSON_UNESCAPED_UNICODE);
        return new JsonResponse($json, $code, [], true);
    }

    /**
     * @Route("/places/category/{id}", name="places_category_id", methods="GET")
     */
    public function places_category_id(Category $category): JsonResponse
    {
        $data = [];
        $code = JsonResponse::HTTP_OK;;

        if (!is_null($category)) {
            $places = [];
            $em = $this->getDoctrine()->getManager();

            foreach ($category->getPlaces() as $place) {
                $p = [
                    'id' => $place->getId(),
                    'name' => $place->getName(),
                    'address' => $place->getAddress(),
                    'postal_code' => $place->getPostalCode(),
                    'city' => $place->getCity(),
                    'latitude' => $place->getLatitude(),
                    'longitude' => $place->getLongitude()
                ];
                array_push($places,$p);
                $data = [
                    'category_id' => $category->getId(),
                    'place' => $places
                ];
            }
        }

        $json = json_encode($data, JSON_UNESCAPED_UNICODE);
        return new JsonResponse($json, $code, [], true);
    }

    /**
     * @Route("/places", name="place_create", methods="POST")
     */
    public function place_create(Request $request): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        $input = json_decode($request->getContent(), true);

        if(!is_null($input['category_id']) && !is_null($input['name']) && !is_null($input['address']) && !is_null($input['postal_code']) && !is_null($input['city'])){
            if(is_numeric($input['category_id'])){
                $samePlaceName = $em->getRepository(Place::class)->findOneBy(['name' => $input['name']]);
                if(is_null($samePlaceName)){
                    $place = new Place();
                    $critere = ['id'=>$input['category_id']];
                    $category = $em->getRepository(Category::class)->findOneBy($critere);
                    $place->setCategory($category);
                    $place->setName($input['name']);
                    $place->setAddress($input['address']);
                    $place->setPostalCode($input['postal_code']);
                    $place->setCity($input['city']);

                    //Find coordinates
                    $result = $this->GetCoordinates($place->getAddress(),$place->getPostalCode(),$place->getCity());
                    $place->setLatitude($result->lat);
                    $place->setLongitude($result->lng);

                    $em->persist($place);
                    $em->flush();

                    $data = [
                        'id' => $place->getId(),
                        'category_id' => $place->getCategory()->getId(),
                        'name' => $place->getName(),
                        'address' => $place->getAddress(),
                        'postal_code' => $place->getPostalCode(),
                        'city' => $place->getCity(),
                        'latitude' => $place->getLatitude(),
                        'longitude' => $place->getLongitude()
                    ];
                    $code = JsonResponse::HTTP_CREATED;
                }
                else{
                    $data = [
                        "msg" => "Already exists"
                    ];
                    $code = JsonResponse::HTTP_CONFLICT;
                }
            }
            else{
                $data = [
                    "msg" => "Non numeric value on category_id"
                ];
                $code = JsonResponse::HTTP_NOT_ACCEPTABLE;
            }
        }
        else{
            $data = [
                "msg" => "Missing property : category_id or name or address, or postal_code or city"
            ];
            $code = JsonResponse::HTTP_BAD_REQUEST;
        }

        $json = json_encode($data, JSON_UNESCAPED_UNICODE);
        return new JsonResponse($json, $code, [], true);
    }

    /**
     * @Route("/places/{id}", name= "place_update", methods="PATCH")
     */
    public function place_update (Place $place, Request $request): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $input = json_decode($request->getContent(), true);
        $data = [];

        if (!is_null($place)){
            $category = $em->getRepository(Category::class)->findOneBy(["id" => $input['category_id']]);
            if(!is_null($category)){
                $place->setName($input['name']);
                $place->setCategory($category);
                $place->setAddress($input['address']);
                $place->setPostalCode($input['postal_code']);
                $place->setCity($input['city']);

                $coordinates = $this->GetCoordinates($place->getAddress(),$place->getPostalCode(),$place->getCity());
                $place->setLatitude($coordinates->lat);
                $place->setLongitude($coordinates->lng);
                
                $em->flush();

                $data = [
                    'id' => $place->getId(),
                    'name'=> $place->getName(),
                    'address' => $place->getAddress(),
                    'postal_code' => $place->getPostalCode(),
                    'city' => $place->getCity(),
                    'latitude' => $place->getLatitude(),
                    'longitude' => $place->getLongitude(),
                    'category_id' => $place->getCategory()->getId()
                ];

                $code = JsonResponse::HTTP_ACCEPTED;
            }
            else{
                $data = [
                    "msg" => "Invalid category_id"
                ];
                $code = JsonResponse::HTTP_BAD_REQUEST;
            }
        }
        
        $json = json_encode($data, JSON_UNESCAPED_UNICODE);
        return new JsonResponse($json, $code, [], true);
    }

    /**
     * @Route("/places/{id}", name="place_delete", methods="DELETE")
     */
    public function place_delete(Place $place): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        if (!is_null($place)){
            $em->remove($place);
            $em->flush();
            $data = [];
            $code = JsonResponse::HTTP_NO_CONTENT;
        }

        $json = json_encode($data, JSON_UNESCAPED_UNICODE);
        return new JsonResponse($json, $code, [], true);
    }
    

    /**
     * @Route("/places/{latitude}/{longitude}/{range}", name="places_range", methods="GET")
     */
    public function places_range($latitude, $longitude, $range)
    {
        $latitude = (float)$latitude;
        $longitude = (float)$longitude;
        $range = (int)$range;

        $placesInRange = [];

        $em = $this->getDoctrine()->getManager();
        $places = $em->getRepository(Place::class)->findAll();

        $dataPlaces = [];

        foreach ($places as $p) {
            $distance = $this->GetDistance($latitude, $longitude, $p->getLatitude(), $p->getLongitude());
            if($distance <= $range){
                $placeToAdd = [
                    'id' => $p->getId(),
                    'name' => $p->getName(),
                    'address' => $p->getAddress(),
                    'postal_code' => $p->getPostalCode(),
                    'city' => $p->getCity(),
                    'latitude' => $p->getLatitude(),
                    'longitude' => $p->getLongitude(),
                    'category_id' => $p->getCategory()->getId(),
                    'distance' => $distance
                ];
                array_push($dataPlaces, $placeToAdd);
            }
        }

        $data = [
            'latitude' => $latitude,
            'longitude' => $longitude,
            'range' => $range,
            'results' => $dataPlaces
        ];
        
        $code = JsonResponse::HTTP_CREATED;
        $json = json_encode($data, JSON_UNESCAPED_UNICODE);
        return new JsonResponse($json, $code, [], true);
    }

    function GetDistance($lat1, $lon1, $lat2, $lon2) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;

        return ($miles * 1.609344);
    }

    function GetCoordinates($address, $postal_code, $city) {
        // Format query string
        $query = $address . ' ' . $postal_code . ' ' . $city;
        // no spaces
        $query = str_replace(" ", "%20", "$query");

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, "https://api.opencagedata.com/geocode/v1/json?q=" . $query . "&key=a490034534f543c7a057d87d8a1c2cef&language=en&pretty=1&no_annotations=1");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = json_decode(curl_exec($curl))->results[0]->geometry;
        curl_close($curl);

        return $result;
    }
}
