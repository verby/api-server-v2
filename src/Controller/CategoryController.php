<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryResponse;

use App\Entity\Category;
use App\Entity\Place;

class CategoryController extends AbstractController
{
    /**
     * @Route("/category", name="category", methods="GET" )
     */
    public function index(): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository(Category::class)->findAll();

        $data = [];
        foreach ($categories as $category) {
            array_push($data, [
                'id' => $category->getId(),
                'name' => $category->getName()
            ]);
        }

        $code = JsonResponse::HTTP_OK;
        $json = json_encode(["category" => $data],JSON_UNESCAPED_UNICODE);
        return new JsonResponse($json, $code, [], true);
    }

    /**
     * @Route("/category/{id}", name="category_id", methods="GET" )
     */
    public function category_id(Category $category): JsonResponse
    {
        $data = [];
        $code = JsonResponse::HTTP_NOT_FOUND;

        if(!is_null($category)){
            $data = [
                'id' => $category->getId(),
                'name' => $category->getName()
            ];
            $code = JsonResponse::HTTP_OK;
        }

        $json = json_encode($data, JSON_UNESCAPED_UNICODE);
        return new JsonResponse($json, $code, [], true);
    }

    /**
     * @Route("/category", name="category_create", methods="POST")
     */
    public function category_create(Request $request): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();

        $input = json_decode($request->getContent(), true);

        $data = [];
        $code;

        if(isset($input['name'])){
            $sameNameCategory = $em->getRepository(Category::class)->findOneBy(['name' => $input['name']]);
            if(is_null($sameNameCategory)){
                $category = new Category();
                $category->setName($input['name']);

                $em->persist($category);
                $em->flush();

                $data = [
                    'id' => $category->getId(),
                    'name'=> $category->getName()
                ];
                $code = JsonResponse::HTTP_CREATED;
            }
            else{
                $data = [
                    "msg" => "Already exists"
                ];
                $code = JsonResponse::HTTP_CONFLICT;
            }
        }
        else{
            $data = [
                "msg" => "No name provided"
            ];
            $code = JsonResponse::HTTP_BAD_REQUEST;
        }

        
        $json = json_encode($data, JSON_UNESCAPED_UNICODE);
        return new JsonResponse($json, $code, [], true);
    }

    /**
     * @Route("/category/{id}", name="category_update", methods="PUT")
     */
    public function category_update(Category $category, Request $request): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $input = json_decode($request->getContent(), true);

        $data = [];
        $code;

        if(!is_null($category)){
            if(isset($input['name'])){
                $sameNameCategory = $em->getRepository(Category::class)->findOneBy(['name' => $input['name']]);
                if(is_null($sameNameCategory)){                
                    $category->setName($input['name']);
                    $em->flush();
                    $data = [
                        'id' => $category->getId(),
                        'name' => $category->getName()
                    ];
                    $code = JsonResponse::HTTP_ACCEPTED;
                }
                else{
                    $data = [
                        "msg" => "Already exists"
                    ];
                    $code = JsonResponse::HTTP_CONFLICT;
                }
            }
            else{
                $data = [
                    "msg" => "No name provided"
                ];
                $code = JsonResponse::HTTP_BAD_REQUEST;
            }
        }

        $json = json_encode($data, JSON_UNESCAPED_UNICODE);
        return new JsonResponse($json, $code, [], true);
    }

    /**
     * @Route("/category/{id}", name="category_delete", methods="DELETE")
     */
    public function category_delete(Category $category): JsonResponse
    {
        $em = $this->getDoctrine()->getManager();
        $data = [];
        $code;

        if (!is_null($category)){
            $places = $em->getRepository(Place::class)->findBy(['category' => $category->getId()]);
            if(sizeof($places) == 0){
                $em->remove($category);
                $em->flush();
                $code = JsonResponse::HTTP_NO_CONTENT;
            }
            else{
                $data = [
                    "msg" => "Category is not empty"
                ];
                $code = JsonResponse::HTTP_CONFLICT;
            }
        }

        $json = json_encode($data, JSON_UNESCAPED_UNICODE);
        return new JsonResponse($json, $code, [], true);
    }
}
