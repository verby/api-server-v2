<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerVL5xoXB\srcDevDebugProjectContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerVL5xoXB/srcDevDebugProjectContainer.php') {
    touch(__DIR__.'/ContainerVL5xoXB.legacy');

    return;
}

if (!\class_exists(srcDevDebugProjectContainer::class, false)) {
    \class_alias(\ContainerVL5xoXB\srcDevDebugProjectContainer::class, srcDevDebugProjectContainer::class, false);
}

return new \ContainerVL5xoXB\srcDevDebugProjectContainer(array(
    'container.build_hash' => 'VL5xoXB',
    'container.build_id' => 'd07a7352',
    'container.build_time' => 1540979517,
), __DIR__.\DIRECTORY_SEPARATOR.'ContainerVL5xoXB');
